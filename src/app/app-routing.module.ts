import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RoomListComponent } from './room-list/room-list.component';
import { RoomCreationComponent } from './room-creation/room-creation.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: { animation: 'isLeft' }
  },
  {
    path: 'create',
    component: RoomCreationComponent,
    data: { animation: 'isRight' }
  },
  {
    path: 'join',
    component: RoomListComponent,
    data: { animation: 'isRight' }
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
