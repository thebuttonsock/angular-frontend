import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { CookieService } from 'ngx-cookie-service';
import { CookieLawModule } from 'angular2-cookie-law';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './store/effects/app.effects';
import { StoreModule } from '@ngrx/store';
import * as fromRoot from '@app/root-store';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RoomCreationComponent } from './room-creation/room-creation.component';
import { RoomListComponent } from './room-list/room-list.component';
import { HomeComponent } from './home/home.component';
import { RoomComponent } from './room/room.component';

const config: SocketIoConfig = { url: 'http://localhost:3000', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    RoomCreationComponent,
    RoomListComponent,
    HomeComponent,
    RoomComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule,
    ChartsModule,
    CookieLawModule,
    SocketIoModule.forRoot(config),
    StoreModule.forRoot(
      fromRoot.reducers,
    ) /* Initialise the Central Store with Application's main reducer*/,
    EffectsModule.forRoot([
      AppEffects,
    ]) /* Start monitoring app's side effects */,
    !environment.production
      ? StoreDevtoolsModule.instrument({ maxAge: 50 })
      : [],
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
  ],
  providers: [CookieService],
  bootstrap: [AppComponent],
})
export class AppModule {}
