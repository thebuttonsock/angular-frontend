import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-room-creation',
  templateUrl: './room-creation.component.html',
  styleUrls: ['./room-creation.component.scss'],
})
export class RoomCreationComponent implements OnInit {
  roomCreationForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.roomCreationForm = this.formBuilder.group({
      roomName: ['', Validators.required],
      roomDescription: ['', Validators.required],
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.roomCreationForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.roomCreationForm.invalid) {
      return;
    }

    // Do Socket Stuff
  }
}
