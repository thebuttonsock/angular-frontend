import { Observable, Subscription } from 'rxjs';
import { RoomListService } from './room-list.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.scss']
})
export class RoomListComponent implements OnInit, OnDestroy {

  private roomListSub: Subscription;

  constructor(private rls: RoomListService) { }

  ngOnInit() {
    this.roomListSub = this.rls.myRooms.subscribe(rooms => {
      console.log(rooms);
    });
    this.rls.findAllRoom();
  }

  ngOnDestroy() {
    this.roomListSub.unsubscribe();
  }

}
