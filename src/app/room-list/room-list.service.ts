import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Room } from '../room/room';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoomListService {

  public myRooms = new Subject<Room>(); // Use decorater to convert into proper objects? @Assign?
  private roomList = this.socket.fromEvent<Room[]>('findAllRoom');
  private newRooms = this.socket.fromEvent<Room>('saveOneRoom');
  private deletedRooms = this.socket.fromEvent<Room>('deleteOneRoom');

  constructor(private socket: Socket) {
    this.roomList.subscribe(rooms => {
      rooms.forEach(r => {
        this.myRooms.next(r);
      });
    });
  }

  findAllRoom() {
    console.log('Looking for Rooms');
    this.socket.emit('findAllRoom');
  }
}
